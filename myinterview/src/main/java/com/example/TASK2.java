package com.example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */

public class TASK2 {

	public static void main(String[] args) {
	    Scanner ler = new Scanner(System.in);

	    // declaring a new list
	    ArrayList<String> list = new ArrayList();

	    // using the add method to write to the list
	    list.add("1");
	    list.add("2");
	    list.add("3");
	    list.add("4");

	    int i;
	    int n = list.size();
	    for (i=0; i<n; i++) {
	      System.out.printf("Posição %d- %s\n", i, list.get(i));
	    }

	    System.out.printf("\nInforme a posição a ser excluída:\n");
	    i = ler.nextInt();

	    try {
	    	list.remove(i);
	    } catch (IndexOutOfBoundsException e) {

	        System.out.printf("\nErro: posição inválida (%s).",
	          e.getMessage());
	    }

	    i = 0;
	    for (String contato: list) {
	      System.out.printf("Posição %d- %s\n", i, contato);
	      i++;
	    }
	  }
}
