package com.example;

import java.util.Scanner;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
	
	private static Scanner ent;

	public static void main(String[] args) {
        // TODO code application logic here
        
        ent = new Scanner(System.in);
        
        String contrario = "";
        
        System.out.println("Digite uma palavra para verificar se é um palindromo");
        String nome = ent.nextLine();

        
        for (int i = (nome.length() -1); i >= 0; i--) {
            contrario = contrario + nome.charAt(i);
        }
        
        
        if (contrario.toLowerCase().equals(nome.toLowerCase())) {
            System.out.println("\nEssa palavra é palindromo :)");
        } else {
            System.out.println("\nEssa palavra nao é palindromo.");
        }
        
    }
    
}
